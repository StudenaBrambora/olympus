﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Mirror;

public class Player : NetworkBehaviour {

    //Public variables
    // settings
    public float zoomSensitivity = 1f;
    public float orbitSensitivity = 0.03f;
    public float camSpeed = 0.2f;

    // prefabs and references
    public GameObject playerFigurePrefab;
    [SyncVar] public GameObject playerModel;
    public GameObject diePrefab;


    //Private variables
    // camera and input
    Camera playerCamera;
    GameObject orbitTarget;
    Transform draggedObject;
    Vector3 orbitLastPosition;
    bool dragging = false;

    // context menu
    Vector3 contextMenuWorldPosition;
    GameObject rightClickedObject;

    // components
    GameSystem gameSystem;
    CustomDataHolder dataHolder;
    GameObject uiCanvas;
    UIManager uiManager;

    // player information
    bool isActivePlayer;
    string playerName;
    public SyncList<Card> playHand = new SyncList<Card>();
    [SyncVar(hook = nameof(UpdateDeckCountText))] public int playHandCount;

    public bool isAtDiceBoard = false;
    Vector3 preDicePos;
    Quaternion preDiceRot;




    void Update() {

        if (isLocalPlayer) {
            if (!isAtDiceBoard) {
                MoveOrbit();
                OrbitCamera();
                ZoomCamera();
            }

            MoveToDiceBoard();

            if (isActivePlayer) {
                DragAndDrop();
                ContextMenu();
                DrawCardFromDeck();
            }
        }
    }

    public override void OnStartLocalPlayer() {

        // setup variables
        playerCamera = transform.GetComponent<Camera>();
        gameSystem = GameObject.FindGameObjectWithTag("Game System").GetComponent<GameSystem>();
        dataHolder = GameObject.FindObjectOfType<CustomDataHolder>();
        if (!gameSystem) { Debug.LogError("Nenašel se GameSystem"); }
        if (!dataHolder) { Debug.LogError("Nenašel se CustomDataHolder"); }




        // get info about the player
        isActivePlayer = dataHolder.isActivePlayer;
        playerName = dataHolder.playerName;

        // send connect message to server
        gameSystem.CmdSendChatMessage($"{playerName} has connected");

        // increment number of players
        if (isActivePlayer) {
            gameSystem.CmdSpawnPlayerModel(transform, playerName);
            playHandCount = playHand.Count;
        }


        // setup a context menu
        uiCanvas = gameSystem.canvas;
        uiCanvas.GetComponent<UIManager>().associatedPlayer = this;
        uiManager = uiCanvas.GetComponent<UIManager>();

        // setup camera
        orbitTarget = new GameObject("Player Orbit");
        orbitTarget.transform.position = new Vector3(0, 0, 0);
        orbitTarget.transform.rotation = Quaternion.Euler(0, 0, 0);
        Camera mainCamera = GetComponent<Camera>();
        mainCamera.transform.SetParent(transform);
        mainCamera.transform.localPosition = Vector3.zero;
        mainCamera.transform.localRotation = Quaternion.Euler(0, 0, 0);
        mainCamera.enabled = true;

        gameSystem.CmdSynchronizePlayerNames();
        CmdSyncPlayerModel(gameObject, gameObject);

        playHand.Callback += UpdateHandUIDelegate;
    }

    public void SpawnFigure(Color color) {
        CmdSpawnFigure(color, contextMenuWorldPosition);
        gameSystem.CmdSendChatMessage($"{playerName} spawned a figure");
    }

    public void DeleteFigure() {
        if (!rightClickedObject) { return; }
        if (rightClickedObject.tag != "Figure") { return; }
        CmdDeleteFigure(rightClickedObject);
        gameSystem.CmdSendChatMessage($"{playerName} deleted a figure");
    }

    public void SpawnDie(string type) {
        CmdSpawnDie(type, contextMenuWorldPosition);
    }

    public void DeleteDie() {
        if (!rightClickedObject) { return; }
        if (rightClickedObject.tag != "Die") { return; }
        CmdDeleteDie(rightClickedObject);
    }

    public void ShuffleDeck() {
        if (!rightClickedObject) { return; }
        if (!rightClickedObject.transform.parent) { return; }
        if (rightClickedObject.transform.parent.tag != "Card Deck") { return; }
        CmdShuffleDeck(rightClickedObject.transform.parent.gameObject);
        gameSystem.CmdSendChatMessage($"{playerName} shuffled a deck");
    }

    void DragAndDrop() {
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && !dragging) {

            Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);

            if (hit.transform && (hit.transform.tag == "Figure"
                                || hit.transform.tag == "Card"
                                || hit.transform.tag == "Die")) {
                dragging = true;
                draggedObject = hit.transform;
                CmdSetObjectCollider(draggedObject, false);
            }
        } else

        if (dragging && !Input.GetMouseButtonUp(0)) {
            Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);
            draggedObject.position = hit.point;
            //CmdSetFigurePosition(draggedObject, hit.point); // don't update every frame, too much network traffic
        } else

        if (dragging && (Input.GetMouseButtonUp(0) || !Input.GetMouseButton(0))) {
            Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);


            if (draggedObject.tag == "Card" && hit.transform.tag == "Player Card Deck") {
                CmdPassCardToPlayer(playerName, draggedObject, hit.transform.parent.parent, gameSystem.gameObject);
                dragging = false;
                return;
            } else if (draggedObject.tag == "Card" && hit.transform.tag == "Card Deck") {
                CmdPassCardToDeck(playerName, draggedObject, hit.transform.parent, gameSystem.gameObject);
                dragging = false;
                return;
            }
            CmdSetObjectCollider(draggedObject, true);
            CmdSetObjectPostion(draggedObject, draggedObject.position);
            dragging = false;


            if (draggedObject.tag == "Figure") {
                gameSystem.CmdSendChatMessage($"{playerName} has moved a figure");
            }
        }
    }

    public void DragObject(Transform obj) {
        draggedObject = obj;
        CmdSetObjectCollider(draggedObject, false);
        dragging = true;
    }

    void OrbitCamera() {

        if (Input.GetMouseButtonDown(2)) {
            orbitLastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(2)) {
            Vector3 delta = Input.mousePosition - orbitLastPosition;
            transform.Translate(delta.x * -orbitSensitivity, delta.y * -orbitSensitivity, 0.0f);
            transform.LookAt(orbitTarget.transform);

            orbitLastPosition = Input.mousePosition;
        }
    }

    void ZoomCamera() {
        if (Input.GetAxis("Mouse ScrollWheel") < 0) {
            transform.Translate(Vector3.back * zoomSensitivity);
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0) {
            transform.Translate(Vector3.forward * zoomSensitivity);
        }

    }

    void MoveOrbit() {
        transform.Translate(transform.right * Input.GetAxis("Horizontal") * camSpeed, Space.World);
        orbitTarget.transform.Translate(transform.right * Input.GetAxis("Horizontal") * camSpeed, Space.World);

        Vector3 vertical = new Vector3(transform.forward.x, 0, transform.forward.z);
        transform.Translate(vertical * Input.GetAxis("Vertical") * camSpeed, Space.World);
        orbitTarget.transform.Translate(vertical * Input.GetAxis("Vertical") * camSpeed, Space.World);

    }

    void MoveToDiceBoard() {
        if (!Input.GetButtonDown("Spacebar")) { return; }

        if (isAtDiceBoard) {
            transform.position = preDicePos;
            transform.rotation = preDiceRot;
            isAtDiceBoard = false;

        } else {
            preDicePos = transform.position;
            preDiceRot = transform.rotation;

            transform.position = new Vector3(5, 5, 15);
            transform.rotation = Quaternion.Euler(90, 0, 0);
            isAtDiceBoard = true;
        }
    }

    void ContextMenu() {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject()) {
            uiManager.CloseContextMenu();
        } else

        if (Input.GetMouseButtonDown(1)) {
            RaycastHit hit;
            Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);

            if (hit.transform != null) {
                if (hit.transform.tag == "Figure"
                    || hit.transform.tag == "Card Deck"
                    || hit.transform.tag == "Die"
                    || hit.transform.tag == "Card") {
                    rightClickedObject = hit.transform.gameObject;
                }

                uiManager.OpenContextMenu(playerCamera.WorldToScreenPoint(hit.point));
                contextMenuWorldPosition = hit.point;
            } else {
                rightClickedObject = null;
                uiManager.CloseContextMenu();
            }
        }
    }

    void DrawCardFromDeck() {
        if (!Input.GetMouseButtonDown(0)) { return; }

        RaycastHit hit;
        Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit);

        if (!hit.transform) { return; }
        if (hit.transform.tag != "Card Deck") { return; }

        hit.transform.parent.GetComponent<CardDeck>().CmdDrawCard(gameObject);
        playHandCount = playHand.Count;
    }

    void UpdateHandUIDelegate(SyncList<Card>.Operation op, int index, Card _new, Card _old) {
        UpdateHandUI();
    }

    public void ToggleCardVisibility() {
        if (!rightClickedObject) { return; }
        if (rightClickedObject.tag != "Card") { return; }
        CmdToggleCard(rightClickedObject, true, gameObject);
    }

    public void EnableCollidersForAllCards() {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Card")) {
            CmdSetObjectCollider(obj.transform, true);
        }
    }

    public void UpdateHandUI() {
        foreach (Transform card in uiCanvas.transform.GetChild(2)) {
            Destroy(card.gameObject);
        }

        int i = 0;
        foreach (Card card in playHand) {
            GameObject cardObj = card.Create2DCard();
            cardObj.transform.parent = uiCanvas.transform.GetChild(2);
            cardObj.GetComponent<CardHolder>().UpdateCard(card, false, gameObject, false);
            cardObj.transform.localPosition = new Vector3(-100 * i, 0, 0);
            cardObj.transform.localRotation = Quaternion.Euler(-90, 0, 0);
            cardObj.transform.localScale = new Vector3(150, 150, 150);
            cardObj.transform.SetAsFirstSibling();
            i++;
        }
        UpdateDeckCount();
    }
    [Command]
    void UpdateDeckCount() {
        playHandCount = playHand.Count;
    }

    //Commands and RP Callbacks
    [Command]
    void CmdSpawnFigure(Color color, Vector3 position) {
        Transform playerFigure = Instantiate(playerFigurePrefab, position, Quaternion.Euler(0, 0, 0)).transform;
        NetworkServer.Spawn(playerFigure.gameObject);
        RpcSetFigureColor(playerFigure, color);
    }

    [Command]
    void CmdDeleteFigure(GameObject figure) {
        NetworkServer.UnSpawn(figure);
        Destroy(figure);
    }

    [Command]
    void CmdSpawnDie(string type, Vector3 position) {
        GameObject die = Instantiate(diePrefab, position, Quaternion.Euler(0, 0, 0));
        bool random = false;
        NetworkServer.Spawn(die);

        if (type == "random") {
            random = true;
            int rnd = Random.Range(1, 5);
            if (rnd <= 2) {
                type = "attack"; ;
            } else if (rnd <= 4) {
                type = "parry"; ;
            } else if (rnd <= 5) {
                type = "riposte"; ;
            } else {
                type = "lunge";
            }
        }

        if (type == "ruins") {
            int rnd = Random.Range(1, 6);
            if (rnd <= 3){
                type = "loot";
            } else if (rnd <= 4){
                type = "fight";
            } else {
                type = "disaster";
            }
        }
        die.GetComponent<Die>().RpcSetDieType(type, random);
    }

    [Command]
    void CmdDeleteDie(GameObject die) {
        NetworkServer.UnSpawn(die);
        Destroy(die);
    }

    [Command]
    void CmdShuffleDeck(GameObject deck) {
        deck.GetComponent<CardDeck>().ShuffleDeck();
    }

    [Command(ignoreAuthority = true)]
    void CmdSetObjectCollider(Transform tObject, bool state) {
        RpcSetObjectCollider(tObject, state);
    }
    [ClientRpc]
    void RpcSetObjectCollider(Transform tObject, bool state) {
        if (!tObject) { return; }
        if (!tObject.GetComponent<Collider>()) { return; }
        tObject.GetComponent<Collider>().enabled = state;
    }

    [Command(ignoreAuthority = true)]
    void CmdSetObjectPostion(Transform tObject, Vector3 newPosition) {
        RpcSetObjectPosition(tObject, newPosition);
    }
    [ClientRpc]
    void RpcSetObjectPosition(Transform tObject, Vector3 newPosition) {
        tObject.position = newPosition + new Vector3(0f, 0.02f, 0f);
        tObject.LookAt(new Vector3(0, tObject.position.y, 0));

        float n = Mathf.Floor((tObject.rotation.eulerAngles.y + 30) / 60) * 60;
        tObject.rotation = Quaternion.Euler(0, n + 180, 0);
    }

    [ClientRpc]
    void RpcSetFigureColor(Transform figure, Color color) { // change thy color biiitch
        figure.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material.SetColor("_Color", color);
    }

    void UpdateDeckCountText(int _old, int _new) { // hook
        playerModel.transform.GetChild(1).GetComponentInChildren<Text>().text = _new.ToString();
    }

    [Command]
    void CmdSyncPlayerModel(GameObject owner, GameObject assocPlayer) { // cuz mirror is retarded
        RpcSyncPlayerModel(owner, assocPlayer, playerModel);
    }
    [ClientRpc]
    void RpcSyncPlayerModel(GameObject owner, GameObject assocPlayer, GameObject model) {
        owner.GetComponent<Player>().playerModel = model;
        playerModel.GetComponent<Counter>().associatedPlayerObject = assocPlayer;
    }

    [Command]
    void CmdPassCardToPlayer(string giverName, Transform card, Transform receiver, GameObject gsObject) {
        Player recPlayer = receiver.GetComponent<Counter>().associatedPlayerObject.GetComponent<Player>();
        recPlayer.playHand.Add(card.GetComponent<CardHolder>().cardInfo);
        recPlayer.playHandCount = recPlayer.playHand.Count;

        gsObject.GetComponent<GameSystem>().CmdSendChatMessage($"{giverName} has passed {recPlayer.playerName} a card");
        NetworkServer.UnSpawn(card.gameObject);
        Destroy(card.gameObject);
    }

    [Command]
    void CmdPassCardToDeck(string giverName, Transform card, Transform receiver, GameObject gsObject) {
        CardDeck recDeck = receiver.GetComponent<CardDeck>();
        recDeck.AddCard(card.GetComponent<CardHolder>().cardInfo);

        string actionString = "";

        switch (recDeck.type) {
            case CardDeck.DeckType.NEW:
                actionString = "has passed a card into new deck";
                break;
            case CardDeck.DeckType.ZEUS:
                actionString = "has passed a card into the Zeus deck";
                break;
            case CardDeck.DeckType.DISCARD:
                actionString = "has discarded a card";
                break;
        }
        gsObject.GetComponent<GameSystem>().CmdSendChatMessage($"{giverName} {actionString}");

        NetworkServer.UnSpawn(card.gameObject);
        Destroy(card.gameObject);
    }

    [Command]
    public void RemoveCard(Card card) {
        playHand.Remove(card);
    }

    [Command]
    public void CmdSpawn3DCard(Card card, bool putInHand, bool hidden) {
        GameObject cardObj = card.Create3DCard();
        NetworkServer.Spawn(cardObj);
        RpcUpdate3DCard(cardObj, card, putInHand, hidden);
    }
    [ClientRpc]
    void RpcUpdate3DCard(GameObject cardObj, Card card, bool putInHand, bool hidden) {
        cardObj.GetComponent<CardHolder>().UpdateCard(card, true, gameObject, hidden);
        if (putInHand) {
            DragObject(cardObj.transform);
        }
    }

    [Command]
    void CmdToggleCard(GameObject cardObj, bool threeD, GameObject playerObject) {
        RpcToggleCard(cardObj, threeD, playerObject);
    }
    [ClientRpc]
    void RpcToggleCard(GameObject cardObj, bool threeD, GameObject playerObject) {
        CardHolder cardHolder = cardObj.transform.GetComponent<CardHolder>();
        cardHolder.UpdateCard(cardHolder.cardInfo, threeD, playerObject, !cardHolder.isHidden);
    }
}

