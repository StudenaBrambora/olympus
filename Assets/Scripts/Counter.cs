﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class Counter : NetworkBehaviour
{

    [SyncVar] public GameObject associatedPlayerObject;

    [SerializeField] int maxHealth;
    [SerializeField, SyncVar(hook = nameof(OnHealthUpdate))] int actualHealth;

    [SerializeField] int maxPrestige;
    [SerializeField, SyncVar(hook = nameof(OnPrestigeUpdate))] int actualPrestige;
    
    Text healthText;
    Text prestigeText;


    void Start(){
        healthText = transform.GetChild(2).GetChild(0).GetComponent<Text>();
        prestigeText = transform.GetChild(2).GetChild(3).GetComponent<Text>();
        transform.GetChild(2).GetComponent<Canvas>().worldCamera = Camera.main;
    }

    [Command(ignoreAuthority = true)] public void OnAddHealth(){
        if(actualHealth + 1 >= maxHealth){
            actualHealth = maxHealth;
        } else{
            actualHealth += 1;
        }
    }

    [Command(ignoreAuthority = true)] public void OnAddPrestige(){
        if(actualPrestige + 1 >= maxPrestige){
            actualPrestige = maxPrestige;
        } else{
            actualPrestige += 1;
        }
    }

    [Command(ignoreAuthority = true)] public void OnSubHealth(){
        if(actualHealth -1 < 0){
            actualHealth = 0;
        } else{
            actualHealth -= 1;
        }
    }

    [Command(ignoreAuthority = true)] public void OnSubPrestige(){
        if(actualPrestige -1 < 0){
            actualPrestige = 0;
        } else{
            actualPrestige -= 1;
        }
    }

    void OnHealthUpdate(int _old, int _new){
        healthText.text = _new.ToString();
    }

    void OnPrestigeUpdate(int _old, int _new){
        prestigeText.text = _new.ToString();
    }
}
