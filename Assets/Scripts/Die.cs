﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class Die : NetworkBehaviour {
    public enum DieType {
        RANDOM,
        ATTACK,
        PARRY,
        RIPOSTE,
        LUNGE,
        RUINS
    }

    public DieType type;

    [SerializeField] Sprite attackSprite;
    [SerializeField] Sprite parrySprite;
    [SerializeField] Sprite riposteSprite;
    [SerializeField] Sprite lungeSprite;
    [SerializeField] Sprite lootSprite;
    [SerializeField] Sprite fightSprite;
    [SerializeField] Sprite disasterSprite;


    [ClientRpc]
    public void RpcSetDieType(string type, bool isRandom) {
        Image img = transform.GetComponentInChildren<Image>();
        if (isRandom) {
            transform.GetChild(1).GetComponent<MeshRenderer>().material.SetColor("_Color", new Color(0, 0, 0));
        }

        switch (type) {
            case "attack":
                img.sprite = attackSprite;
                this.type = DieType.ATTACK;
                break;
            case "parry":
                img.sprite = parrySprite;
                this.type = DieType.PARRY;
                break;
            case "riposte":
                img.sprite = riposteSprite;
                this.type = DieType.RIPOSTE;
                break;
            case "lunge":
                img.sprite = lungeSprite;
                this.type = DieType.LUNGE;
                break;
            case "loot":
                img.sprite = lootSprite;
                this.type = DieType.RUINS;
                break;
            case "fight":
                img.sprite = fightSprite;
                this.type = DieType.RUINS;
                break;
            case "disaster":
                img.sprite = disasterSprite;
                this.type = DieType.RUINS;
                break;
        }
    }


}
