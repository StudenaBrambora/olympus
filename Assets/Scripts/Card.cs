﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;



[CreateAssetMenu(fileName = "New Card", menuName = "Card Data")]
public class Card : ScriptableObject
{
    public enum Rarity
    {
        COMMON,
        UNCOMMON,
        RARE,
        UNIQUE
    }

    public enum Type
    {
        BATTLE,
        EQUIPMENT,
        FOLLOWER,
        INSTANT,
        ZEUS
    }

    // card info
    public int id;
    public string label;
    public string description;
    public string flavourText;
    public int count;
    public Rarity rarity;
    public Type type;

    // references
    [SerializeField] GameObject prefab3D;
    [SerializeField] GameObject prefab2D;

    public GameObject Create3DCard(){
        return CreateCard(true);
    }
    public GameObject Create2DCard(){
        return CreateCard(false);
    }
    private GameObject CreateCard(bool threeD){
        if(threeD){
            return Instantiate(prefab3D);
        }
        return Instantiate(prefab2D);
    }

}

public static class CardSerializer
{
    public static void WriteCard(this NetworkWriter writer, Card card){
        writer.WriteString(card.id.ToString());
    }

    public static Card ReadCard(this NetworkReader reader){
        return Resources.Load<Card>("Cards/" + reader.ReadString());
    }



}
