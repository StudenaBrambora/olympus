﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class CardHolder : MonoBehaviour
{
    public Card cardInfo;
    public Vector3 moveOnHover = new Vector3(0, 100, 0);
    public Color zeusModelColor = new Color(0.843f, 0.937f, 0.976f);
    public Color zeusTextColor = new Color(0, 0, 0);

    public Sprite battleSprite;
    public Sprite battleBackSprite;
    public Sprite equipmentSprite;
    public Sprite equipmentBackSprite;
    public Sprite followerSprite;
    public Sprite followerBackSprite;
    public Sprite instantSprite;
    public Sprite instantBackSprite;
    public Sprite zeusSprite;
    public Sprite zeusBackSprite;

    public Player player;
    public bool isHidden;


    Canvas canvas;

    void Start(){
        canvas = GetComponentInChildren<Canvas>();
    }

    public void OnHoverEnter(){
        canvas.overrideSorting = true;
        canvas.sortingOrder = 99;
        transform.localPosition += moveOnHover;
    }

    public void OnHoverExit(){
        canvas.sortingOrder = 0;
        transform.localPosition -= moveOnHover;
    }

    public void OnDragOut(){
        player.RemoveCard(cardInfo);
        player.CmdSpawn3DCard(cardInfo, true, Input.GetButton("Left Shift"));
        
        Destroy(gameObject);

    }

    public void SetBackground(Card.Type type){
        Transform canvasTrans = transform.GetChild(0);

    }

    public void UpdateCard(Card card, bool threeD, GameObject playerObject, bool hidden){
        string[] flavour = card.flavourText.Split('-');
        Transform canvas = transform.GetChild(0);
        cardInfo = card;
        isHidden = hidden;

        if(playerObject){
            player = playerObject.GetComponent<Player>();
        } else{
            GetComponentInChildren<EventTrigger>().enabled = false;
        }

        if(hidden){
            switch (card.type){
            case Card.Type.BATTLE:
                canvas.GetChild(0).GetComponent<Image>().sprite = battleBackSprite;
                break;
            case Card.Type.EQUIPMENT:
                canvas.GetChild(0).GetComponent<Image>().sprite = equipmentBackSprite;
                break;
            case Card.Type.FOLLOWER:
                canvas.GetChild(0).GetComponent<Image>().sprite = followerBackSprite;
                break;
            case Card.Type.INSTANT:
                canvas.GetChild(0).GetComponent<Image>().sprite = instantBackSprite;
                break;
            case Card.Type.ZEUS:
                canvas.GetChild(0).GetComponent<Image>().sprite = zeusBackSprite;
                if(threeD){
                    transform.GetChild(1).GetComponent<MeshRenderer>().material.SetColor("_Color", zeusModelColor);  
                }
                break;
            }
            canvas.GetChild(1).GetComponent<TextMeshProUGUI>().SetText("");
            canvas.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("");
            canvas.GetChild(3).GetComponent<TextMeshProUGUI>().SetText("");
            canvas.GetChild(4).GetComponent<TextMeshProUGUI>().SetText("");
            return;
        }
        
        
        canvas.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(card.label);
        if(flavour.Length > 1){
            
            for(int i = 0; i < flavour.Length - 1; i++){
                canvas.GetChild(2).GetComponent<TextMeshProUGUI>().SetText(flavour[i]);
            }
            canvas.GetChild(3).GetComponent<TextMeshProUGUI>().SetText(flavour[flavour.Length - 1]);
        } else{
            if(card.type == Card.Type.ZEUS){
                canvas.GetChild(4).GetComponent<TextMeshProUGUI>().SetText(card.flavourText);
            } else{
                canvas.GetChild(2).GetComponent<TextMeshProUGUI>().SetText(card.flavourText);
            }
            
        }

        if(card.type == Card.Type.ZEUS){
            canvas.GetChild(2).GetComponent<TextMeshProUGUI>().SetText(card.description);
            canvas.GetChild(2).GetComponent<TextMeshProUGUI>().color = zeusTextColor;
        } else{
            canvas.GetChild(4).GetComponent<TextMeshProUGUI>().SetText(card.description);
        }
        

        switch (card.type){
            case Card.Type.BATTLE:
                canvas.GetChild(0).GetComponent<Image>().sprite = battleSprite;
                break;
            case Card.Type.EQUIPMENT:
                canvas.GetChild(0).GetComponent<Image>().sprite = equipmentSprite;
                break;
            case Card.Type.FOLLOWER:
                canvas.GetChild(0).GetComponent<Image>().sprite = followerSprite;
                break;
            case Card.Type.INSTANT:
                canvas.GetChild(0).GetComponent<Image>().sprite = instantSprite;
                break;
            case Card.Type.ZEUS:
                canvas.GetChild(0).GetComponent<Image>().sprite = zeusSprite;
                if(threeD){
                    transform.GetChild(1).GetComponent<MeshRenderer>().material.SetColor("_Color", zeusModelColor);  
                }
                break;
        }
    }


}
