﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public Player associatedPlayer;
    
    GameObject currentlyOpenSubmenu;

    GameObject chat;
    GameObject contextMenu;

    Dictionary<string, Color> colors = new Dictionary<string, Color>(){
        {"red", new Color(1, 0, 0, 1)},
        {"brown", new Color(0.48f, 0.30f, 0, 1)},
        {"orange", new Color(1, 0.58f, 0, 1)},
        {"yellow", new Color(1, 0.96f, 0, 1)},
        {"pink", new Color(1, 0, 0.5f, 1)},
        {"purple", new Color(0.8f, 0, 1, 1)},
        {"blue", new Color(0, 0.23f, 1, 1)},
        {"cyan", new Color(0, 0.96f, 1, 1)},
        {"green", new Color(0, 0.5f, 0.05f, 1)},
        {"lime", new Color(0, 1, 0.1f, 1)},
        {"white", new Color(0.98f, 0.98f, 0.98f, 1)}
    };

    void Start(){
       currentlyOpenSubmenu = null;

       chat = transform.GetChild(0).gameObject;
       contextMenu = transform.GetChild(1).gameObject;
       contextMenu.SetActive(false);
   }

    void CloseOpenSubmenu(){
        if(currentlyOpenSubmenu != null){
            currentlyOpenSubmenu.SetActive(false);
        }
    }

    public void OpenContextMenu(Vector3 screenPosition){
        contextMenu.SetActive(true);
        contextMenu.GetComponent<RectTransform>().position = screenPosition;
    }

    public void CloseContextMenu(){
       contextMenu.SetActive(false);
       if(currentlyOpenSubmenu != null){currentlyOpenSubmenu.SetActive(false);}
   }

    public void ToggleChat(){
       chat.SetActive(!chat.activeSelf);
       CloseContextMenu();
   }

    public void ToggleSpawnFigureSubmenu(){
        CloseOpenSubmenu();
        GameObject submenu = contextMenu.transform.GetChild(0).GetChild(1).gameObject;
        submenu.SetActive(!submenu.activeSelf);
        currentlyOpenSubmenu = submenu;
   }

    public void SpawnFigure(string color){
       CloseContextMenu();
       Color colorToUse = Color.black;
       colors.TryGetValue(color, out colorToUse);
       associatedPlayer.SpawnFigure(colorToUse);
   }

    public void DeleteFigure(){
        associatedPlayer.DeleteFigure();
        CloseContextMenu();
    }

    public void ToggleSpawnDiceSubmenu(){
        CloseOpenSubmenu();
        GameObject submenu = contextMenu.transform.GetChild(2).GetChild(1).gameObject;
        submenu.SetActive(!submenu.activeSelf);
        currentlyOpenSubmenu = submenu;
    }

    public void SpawnDie(string type){
        associatedPlayer.SpawnDie(type);
        CloseContextMenu();
    }

    public void DeleteDie(){
        associatedPlayer.DeleteDie();
        CloseContextMenu();
    }

    public void UpdateUI(){
        associatedPlayer.UpdateHandUI();
        CloseContextMenu();
    }

    public void ShuffleDeck(){
        associatedPlayer.ShuffleDeck();
        CloseContextMenu();
    }

    public void UnstuckCards(){
        associatedPlayer.EnableCollidersForAllCards();
        CloseContextMenu();
    }

    public void ToggleMiscSubmenu(){
        CloseOpenSubmenu();
        GameObject submenu = contextMenu.transform.GetChild(5).GetChild(1).gameObject;
        submenu.SetActive(!submenu.activeSelf);
        currentlyOpenSubmenu = submenu;
    }

    public void ToggleCardVisibility(){
        associatedPlayer.ToggleCardVisibility();
        CloseContextMenu();
    }
}
