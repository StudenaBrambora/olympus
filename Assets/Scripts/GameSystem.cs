﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class GameSystem : NetworkBehaviour
{
    //Public variables
    // settings
    public int playerCount = 0;
    public int maxChatMessageCount = 5;

    // prefabs and references
    public GameObject playerModelPrefab;
    public GameObject canvas;
    public Text playerNameText;


    //Private variables
    // info
    Dictionary<GameObject, string> playerNames = new Dictionary<GameObject, string>();

    
    [SerializeField]
    Transform spawnPoints;
    Text textField;
    int chatMessageCount;



    void Start(){
        textField = canvas.transform.GetChild(0).GetComponent<Text>();
        chatMessageCount = 0;
    }

    [Command(ignoreAuthority = true)] public void CmdSpawnPlayerModel(Transform playerScriptObject, string playerName){
        // spawn player
        GameObject player = Instantiate(playerModelPrefab, spawnPoints.GetChild(playerCount).position, spawnPoints.GetChild(playerCount).rotation);
        NetworkServer.Spawn(player);
        playerNames.Add(player, playerName);
        playerNameText = player.GetComponentInChildren<Text>();
        
        playerCount += 1;

        playerScriptObject.GetComponent<Player>().playerModel = player;
    }

    [Command(ignoreAuthority = true)] public void CmdSynchronizePlayerNames(){
        foreach(var pair in playerNames){
            RpcSynchronizeName(pair.Key, pair.Value);
        }
    }
    [ClientRpc] void RpcSynchronizeName(GameObject player, string name){
        player.GetComponentInChildren<Text>().text = name;
    }

    [ClientRpc] private void RpcSendChatMessage(string message){
        if(chatMessageCount >= maxChatMessageCount){
            textField.text = "";
            chatMessageCount = 0;
        }
        textField.text += message + "\n";
        chatMessageCount++;
    }

    [Command(ignoreAuthority = true)] public void CmdSendChatMessage(string message){
        RpcSendChatMessage(message);
    }

}