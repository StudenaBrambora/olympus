﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class CardDeck : NetworkBehaviour
{
    public enum DeckType
    {
        NEW,
        DISCARD,
        ZEUS,
        DISCARDZEUS
    }

    // Public variables
    public Text deckCountText;
    public DeckType type;

    // Private variables
    [SyncVar(hook = nameof(UpdateDeckCountText))] int deckCount;
    public List<Card> deck;    // server only

    [Server] public override void OnStartServer(){
        CmdSetupDeck();

    }

    [Command(ignoreAuthority = true)] public void CmdDrawCard(GameObject playerObject){
        if(deck.Count == 0){return;}

        Player player = playerObject.GetComponent<Player>();
        player.playHand.Add(deck[deck.Count - 1]);
        player.playHandCount = player.playHand.Count;
        deck.RemoveAt(deck.Count - 1);
        deckCount = deck.Count;
        if(type == DeckType.DISCARD){
            NetworkServer.UnSpawn(transform.GetChild(1).GetChild(0).gameObject);
            Destroy(transform.GetChild(1).GetChild(0).gameObject);

            if(deck.Count < 1){return;}
            Card card = deck[deck.Count - 1];
            GameObject cardObj = card.Create2DCard();

            NetworkServer.Spawn(cardObj);
            RpcUpdateCard(cardObj, card);
        }

    }

    void UpdateDeckCountText(int _old, int _new){
        deckCountText.text = _new.ToString();
    }

    [Server] public void ShuffleDeck(){
        if(type == DeckType.DISCARD){
            transform.parent.GetChild(0).GetComponent<CardDeck>().AddCards(deck);
            deck.Clear();
            Shuffle(transform.parent.GetChild(0).GetComponent<CardDeck>().deck);
            transform.parent.GetChild(0).GetComponent<CardDeck>().deckCount = transform.parent.GetChild(0).GetComponent<CardDeck>().deck.Count;
        }
        else if(type == DeckType.DISCARDZEUS){
            transform.parent.GetChild(2).GetComponent<CardDeck>().AddCards(deck);
            deck.Clear();
            Shuffle(transform.parent.GetChild(2).GetComponent<CardDeck>().deck);
            transform.parent.GetChild(2).GetComponent<CardDeck>().deckCount = transform.parent.GetChild(2).GetComponent<CardDeck>().deck.Count;
        }
        Shuffle(deck);
        deckCount = deck.Count;
    }

    [Server] public void AddCards(List<Card> toAdd){
        deck.AddRange(toAdd);
    }

    [Server] static void Shuffle<Card>(List<Card> cards){
        int n = cards.Count;
        for(int i = 0; i < n - 1; i++){
            int rng = UnityEngine.Random.Range(i, n);
            Card tmp = cards[i];
            cards[i] = cards[rng];
            cards[rng] = tmp;
        }
    }

    [Server] void CmdSetupDeck(){
        deck = new List<Card>();
        Card[] loadedCards = {};

        if(type == DeckType.DISCARD || type == DeckType.DISCARDZEUS){
            deckCount = 0;
            UpdateDeckCountText(0, 0);
            return;
        }

        loadedCards = Resources.LoadAll<Card>("Cards");
        bool isZeus = type == DeckType.ZEUS;

        foreach(Card card in loadedCards){
            if(!(isZeus && card.type == Card.Type.ZEUS) && !(!isZeus && card.type != Card.Type.ZEUS)){
                continue;
            }

            for (int i = 1; i <= card.count; i++){
                
                deck.Add(card);
            }
        }

        Shuffle(deck);
        deckCount = deck.Count;
        UpdateDeckCountText(0, deck.Count);
    }

    [Server] public void AddCard(Card card){
        deck.Add(card);
        deckCount = deck.Count;

        if(type == DeckType.DISCARD){
            if(transform.GetChild(1).childCount > 2){
                NetworkServer.UnSpawn(transform.GetChild(1).GetChild(0).gameObject);
                Destroy(transform.GetChild(1).GetChild(0).gameObject);
            }
            GameObject cardObj = card.Create2DCard();
            NetworkServer.Spawn(cardObj);
            RpcUpdateCard(cardObj, card);
        }
    }

    [ClientRpc] void RpcUpdateCard(GameObject cardObj, Card card){
        cardObj.transform.parent = transform.GetChild(1);
        cardObj.transform.localPosition = Vector3.zero;
        cardObj.transform.localScale = Vector3.one;
        cardObj.transform.SetAsFirstSibling();
        cardObj.GetComponent<CardHolder>().UpdateCard(card, false, null, false);
    }


}
